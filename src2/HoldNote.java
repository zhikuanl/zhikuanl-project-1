import bagel.Image;

public class HoldNote extends Note {
    private final static String HOLD_NOTE_LEFT_IMAGE = "res/holdNoteLeft.png";
    private final static String HOLD_NOTE_RIGHT_IMAGE = "res/holdNoteRight.png";
    private final static String HOLD_NOTE_UP_IMAGE = "res/holdNoteUp.png";
    private final static String HOLD_NOTE_DOWN_IMAGE = "res/holdNoteDown.png";

    public HoldNote(int frame, Lane lane) {
        super(frame, lane);

        switch (lane.getLaneType()) {
            case LEFT: image = new Image(HOLD_NOTE_LEFT_IMAGE); break;
            case RIGHT: image = new Image(HOLD_NOTE_RIGHT_IMAGE); break;
            case UP: image = new Image(HOLD_NOTE_UP_IMAGE); break;
            case DOWN: image = new Image(HOLD_NOTE_DOWN_IMAGE); break;
        }
    }
}

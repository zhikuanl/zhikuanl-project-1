import bagel.Image;
import bagel.Window;

public class Lane {
    private final Image LANELEFT_IMAGE = new Image("res/laneleft.png");
    private final Image LANERIGHT_IMAGE = new Image("res/laneright.png");
    private final Image LANEUP_IMAGE = new Image("res/laneup.png");
    private final Image LANEDOWN_IMAGE = new Image("res/lanedown.png");
    private LaneType type;
    private double x;

    public Lane(LaneType type, double x) {
        this.type = type;
        this.x = x;
    }

    public void render() {
        Image image = null;
        switch (type) {
            case LEFT: image = LANELEFT_IMAGE; break;
            case RIGHT: image = LANERIGHT_IMAGE; break;
            case UP: image = LANEUP_IMAGE; break;
            case DOWN: image = LANEDOWN_IMAGE; break;
        }

        image.draw(x, Window.getHeight() / 2.0);
    }

    public LaneType getLaneType() {
        return type;
    }

    public double getX() {
        return x;
    }
}

enum LaneType {
    LEFT, RIGHT, UP, DOWN
}
import bagel.Font;

public class GameEnd {
    private final static String FONT_FILE = "res/FSO8BITR.TTF";
    private final static Font MESSAGW_FONT = new Font(FONT_FILE, 64);
    private final static String WIN_MESSAGE = "CLEAR!";
    private final static String LOSE_MESSAGE = "TRY AGAIN";
    private final static int WIN_MESSAGE_X = 360;
    private final static int LOSE_MESSAGE_X = 300;
    private final static int MESSAGE_Y = 250;

    public void render(boolean gameWon) {
        if (gameWon) {
            MESSAGW_FONT.drawString(WIN_MESSAGE, WIN_MESSAGE_X, MESSAGE_Y);
        } else {
            MESSAGW_FONT.drawString(LOSE_MESSAGE, LOSE_MESSAGE_X, MESSAGE_Y);
        }
    }
}

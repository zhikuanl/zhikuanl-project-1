import bagel.Image;

public class NormalNote extends Note {
    private final static String NORMAL_NOTE_LEFT_IMAGE = "res/noteLeft.png";
    private final static String NORMAL_NOTE_RIGHT_IMAGE = "res/noteRight.png";
    private final static String NORMAL_NOTE_UP_IMAGE = "res/noteUp.png";
    private final static String NORMAL_NOTE_DOWN_IMAGE = "res/noteDown.png";

    public NormalNote(int frame, Lane lane) {
        super(frame, lane);

        switch (lane.getLaneType()) {
            case LEFT: image = new Image(NORMAL_NOTE_LEFT_IMAGE); break;
            case RIGHT: image = new Image(NORMAL_NOTE_RIGHT_IMAGE); break;
            case UP: image = new Image(NORMAL_NOTE_UP_IMAGE); break;
            case DOWN: image = new Image(NORMAL_NOTE_DOWN_IMAGE); break;
        }
    }
}

import bagel.Font;
import bagel.Input;
import bagel.Keys;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GameWorld {
    private final static String FONT_FILE = "res/FSO8BITR.TTF";
    private final Font SCORE_FONT = new Font(FONT_FILE, 30);
    private final Font FRAME_FONT = new Font(FONT_FILE, 30);
    private List<Lane> lanes = new ArrayList<>();
    private List<Note> notes = new ArrayList<>();
    private ScoreMessage scoreMessage;
    private final static int SCORE_X = 35;
    private final static int SCORE_Y = 35;
    private int score = 0;
    public void render() {
        renderScore();
        renderLanes();
        renderNotes();
        renderScoreMessage();

        FRAME_FONT.drawString("Frame " + ShadowDance.frame,
                800,
                35);
    }

    public void addLane(Lane lane) {
        lanes.add(lane);
    }

    public void addNote(String laneType, String noteType, String frame) {
        int f = Integer.parseInt(frame);
        Lane l = getLane(LaneType.valueOf(laneType.toUpperCase()));

        Note note = null;
        if (noteType.equals("Normal")) {
            note = new NormalNote(f, l);
        } else if (noteType.equals("Hold")) {
            note = new HoldNote(f, l);
        }

        notes.add(note);
    }

    public boolean IsGameOver() {
        return notes.isEmpty();
//        if (notes.isEmpty()) {
//            return true;
//        }
//        return false;
    }

    public void receiveInput(Input input) {
        if (input.wasPressed(Keys.LEFT)) {
            Note note = getFirstNote(LaneType.LEFT);
            if (note instanceof NormalNote) {
                note.isPressed = true;
                updateScore(Math.abs(note.y - 657));
                if (note.frame <= ShadowDance.frame) {
                    notes.remove(note);
                }
            } else if (note instanceof HoldNote) {
                note.isPressed = true;
                updateScore(Math.abs(note.y + 82 - 657));
            }

        }
        if (input.wasPressed(Keys.RIGHT)) {
            Note note = getFirstNote(LaneType.RIGHT);
            if (note instanceof NormalNote) {
                note.isPressed = true;
                updateScore(Math.abs(note.y - 657));
                if (note.frame <= ShadowDance.frame) {
                    notes.remove(note);
                }
            } else if (note instanceof HoldNote) {
                note.isPressed = true;
                updateScore(Math.abs(note.y + 82 - 657));
            }
        }
        if (input.wasPressed(Keys.UP)) {
            Note note = getFirstNote(LaneType.UP);
            if (note instanceof NormalNote) {
                note.isPressed = true;
                updateScore(Math.abs(note.y - 657));
                if (note.frame <= ShadowDance.frame) {
                    notes.remove(note);
                }
            } else if (note instanceof HoldNote) {
                note.isPressed = true;
                updateScore(Math.abs(note.y + 82 - 657));
            }
        }
        if (input.wasPressed(Keys.DOWN)) {
            Note note = getFirstNote(LaneType.DOWN);
            if (note instanceof NormalNote) {
                note.isPressed = true;
                updateScore(Math.abs(note.y - 657));
                if (note.frame <= ShadowDance.frame) {
                    notes.remove(note);
                }
            } else if (note instanceof HoldNote) {
                note.isPressed = true;
                updateScore(Math.abs(note.y + 82 - 657));
            }
        }

        if (input.wasReleased(Keys.LEFT)) {
            Note note = getFirstNote(LaneType.LEFT);
            if (note instanceof HoldNote) {
                updateScore(Math.abs(note.y - 82 - 657));
                if (note.frame <= ShadowDance.frame) {
                    notes.remove(note);
                }
            }
        }
        if (input.wasReleased(Keys.RIGHT)) {
            Note note = getFirstNote(LaneType.RIGHT);
            if (note instanceof HoldNote) {
                updateScore(Math.abs(note.y - 82 - 657));
                if (note.frame <= ShadowDance.frame) {
                    notes.remove(note);
                }
            }
        }
        if (input.wasReleased(Keys.UP)) {
            Note note = getFirstNote(LaneType.UP);
            if (note instanceof HoldNote) {
                updateScore(Math.abs(note.y - 82 - 657));
                if (note.frame <= ShadowDance.frame) {
                    notes.remove(note);
                }
            }
        }
        if (input.wasReleased(Keys.DOWN)) {
            Note note = getFirstNote(LaneType.DOWN);
            if (note instanceof HoldNote) {
                updateScore(Math.abs(note.y - 82 - 657));
                if (note.frame <= ShadowDance.frame) {
                    notes.remove(note);
                }
            }
        }
    }

    public boolean GameWon() {
        return score >= 150;
    }

    private void renderScore() {
        SCORE_FONT.drawString("SCORE " + score,
                SCORE_X,
                SCORE_Y);
    }

    private void renderLanes() {
        for (Lane lane : lanes) {
            lane.render();
        }
    }

    private void renderNotes() {
        Iterator<Note> iterator = notes.iterator();
        while (iterator.hasNext()) {
            Note note = iterator.next();

            if (!note.IsAlive()) {
                if (note.isPressed == false) {
                    updateScore(200);
                }
                iterator.remove();
                continue;
            }

            note.render();
        }
//        for (Note note : notes) {
//            if (!note.IsAlive()) {
//                notes.remove(note);
//                continue;
//            }
//            note.render();
//        }
    }

    private Lane getLane(LaneType type) {
        for (Lane lane : lanes) {
            if (lane.getLaneType() == type) {
                return lane;
            }
        }

        return null;
    }

    private Note getFirstNote(LaneType type) {
        for (Note note : notes) {
            if (note.lane.getLaneType() == type) {
                return note;
            }
        }

        return null;
    }

    private void updateScore(double distance) {
        if (distance <= 15) {
            score += 10;
            scoreMessage = new ScoreMessage("PERFECT");
        } else if (distance <= 50) {
            score += 5;
            scoreMessage = new ScoreMessage("GOOD");
        } else if (distance <= 100) {
            score -= 1;
            scoreMessage = new ScoreMessage("BAD");
        } else {
            score -= 5;
            scoreMessage = new ScoreMessage("MISS");
        }
    }

    private void renderScoreMessage() {
        if (scoreMessage == null) {
            return;
        }

        scoreMessage.render();
    }
}

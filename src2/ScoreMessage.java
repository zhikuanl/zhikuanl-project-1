import bagel.Font;
import bagel.Image;

public class ScoreMessage {
    private final static String FONT_FILE = "res/FSO8BITR.TTF";
    private final Font SCORE_MESSAGE_FONT = new Font(FONT_FILE, 40);
    private int remainingFrame = 30;
    private String message;

    public ScoreMessage(String message) {
        this.message = message;
    }

    public void render() {
        if (remainingFrame == 0) {
            return;
        }

        remainingFrame--;
        SCORE_MESSAGE_FONT.drawString(message, 450, 35);
    }
}

import bagel.*;

import java.io.BufferedReader;
import java.io.FileReader;
/**
 * Skeleton Code for SWEN20003 Project 1, Semester 2, 2023
 * Please enter your name below
 * zhikuanl
 */
public class ShadowDance extends AbstractGame  {
    public static int frame = 0;
    private final static int WINDOW_WIDTH = 1024;
    private final static int WINDOW_HEIGHT = 768;
    private final static String GAME_TITLE = "SHADOW DANCE";
    private final Image BACKGROUND_IMAGE = new Image("res/background.png");
    private final static String WORLD_FILE = "res/level1.csv";
    private final GameStart gameStart = new GameStart();
    private final GameWorld gameWorld = new GameWorld();
    private final GameEnd gameEnd = new GameEnd();
    private boolean started = false;

    public ShadowDance(){
        super(WINDOW_WIDTH, WINDOW_HEIGHT, GAME_TITLE);
    }

    /**
     * Method used to read file and create objects (you can change
     * this method as you wish).
     */
    private void readCSV() {
        try (BufferedReader br = new BufferedReader(new FileReader(WORLD_FILE))) {
            String line;

            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");

                if (data[0].equals("Lane")) {
                    Lane lane = new Lane(LaneType.valueOf(data[1].toUpperCase()), Double.parseDouble(data[2]));
                    gameWorld.addLane(lane);
                } else {
                    gameWorld.addNote(data[0], data[1], data[2]);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * The entry point for the program.
     */
    public static void main(String[] args) {
        ShadowDance game = new ShadowDance();
        game.readCSV();
        game.run();
    }

    /**
     * Performs a state update.
     * Allows the game to exit when the escape key is pressed.
     */
    @Override
    protected void update(Input input) {
        if (input.wasPressed(Keys.ESCAPE)){
            Window.close();
        }
        if (input.wasPressed(Keys.SPACE)) {
            started = true;
        }

        BACKGROUND_IMAGE.draw(Window.getWidth()/2.0, Window.getHeight()/2.0);
        if (gameWorld.IsGameOver()) {
            gameEnd.render(gameWorld.GameWon());
            return;
        }

        if (!started) {
            gameStart.render();
        } else {
            frame++;
            gameWorld.render();
            gameWorld.receiveInput(input);
        }
    }
}

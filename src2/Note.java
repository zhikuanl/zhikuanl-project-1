import bagel.Image;
import bagel.Window;

public class Note {
    protected int frame;
    protected Lane lane;
    protected Image image;
    protected double x;
    protected double y = 100;
    protected boolean isPressed = false;

    public Note(int frame, Lane lane) {
        this.frame = frame;
        this.x = lane.getX();
        this.lane = lane;
    }

    public void render() {
        if (frame >= ShadowDance.frame) {
            return;
        }

        image.draw(x, y);
        drop();
    }

    public void drop() {
        y += 2;
    }

    public boolean IsAlive() {
        double topY = y - (image.getHeight() / 2);
        return topY <= Window.getHeight();
    }
}

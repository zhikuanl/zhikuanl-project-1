import bagel.Font;

public class GameStart {
    private final static String FONT_FILE = "res/FSO8BITR.TTF";
    private final Font TITLE_FONT = new Font(FONT_FILE, 64);
    private final static int TITLE_X = 220;
    private final static int TITLE_Y = 250;
    private final Font INSTRUCTION_FONT = new Font(FONT_FILE, 24);
    private final static String INSTRUCTION_LINE_ONE = "PRESS SPACE TO START";
    private final static String INSTRUCTION_LINE_TWO = "USE ARROW KEYS TO PLAY";

    public void render() {
        TITLE_FONT.drawString("SHADOW DANCE", TITLE_X, TITLE_Y);
        INSTRUCTION_FONT.drawString(INSTRUCTION_LINE_ONE, TITLE_X + 100, TITLE_Y + 190);
        INSTRUCTION_FONT.drawString(INSTRUCTION_LINE_TWO, TITLE_X + 85, TITLE_Y + 220);
    }
}
